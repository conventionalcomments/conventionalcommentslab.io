#!/usr/bin/env sh

# Ensure the user provides a directory as an argument
if [ $# -ne 2 ]; then
    echo "Usage: $0 <directory> <max-size>"
    exit 1
fi

DIRECTORY=$1
LIMIT=$2

# Check if the provided argument is a valid directory
if [ ! -d "$DIRECTORY" ]; then
    echo "Error: $DIRECTORY is not a valid directory."
    exit 1
fi

# Get the size of the directory in kilobytes
DIR_SIZE=$(du -sk "$DIRECTORY" | awk '{print $1}')

# Compare the size against the limit
if [ "$DIR_SIZE" -gt "$LIMIT" ]; then
    echo "Error: Size of '$DIRECTORY' directory ($DIR_SIZE KB) exceeds the $LIMIT KB limit."
    exit 1
else
    echo "Size of '$DIRECTORY' directory is within the $LIMIT KB limit: $DIR_SIZE KB."
fi
