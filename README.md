# Conventional Comments

This is the static site for [Conventional Comments][1].

## How do I run this thing?

### Prerequisites

- You will need to install [Hugo][2] locally. **Important:** install the "extended" version if relevant to your OS.
- You will need to install [Yarn][3].

It is recommended to use [asdf](https://asdf-vm.com/) for this, and to simply run:

```shell
asdf plugin add yarn
asdf plugin add hugo
asdf install
```

### Start local server

```shell
yarn
yarn start
```

[1]: https://conventionalcomments.org/
[2]: https://gohugo.io/getting-started/installing/
[3]: https://classic.yarnpkg.com/en/
