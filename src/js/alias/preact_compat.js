// There's an issue with aliasing JavaScript named exports so this is a workaround
// https://en.parceljs.org/module_resolution.html#javascript-named-exports
module.exports = require('preact/compat');
